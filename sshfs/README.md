# Copy file

```bash
cp ./sshfs-mount-devdan.service ~/.config/systemd/user/sshfs-devdan.service
```

```bash
systemctl --user daemon-reload
```

```bash
systemctl --user start sshfs-devdan
```

```bash
systemctl --user enable sshfs-devdan
```
