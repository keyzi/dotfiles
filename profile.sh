# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# Config default. Trebuie inclus de toate celelalte.

export DOTFILES="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

export USERDATA="/data/$USER" # Aici vor fi de fapt Desktop,Downloads...

#export PATH="$PATH:/home/keyzi/.komodoide/12.0/XRE/state" # ActiveState State Tool

# xdg
# export XDG_CONFIG_HOME="$HOME/.dotfiles/.config"
export XDG_MUSIC_DIR="$USERDATA/Music"

# XDG_DESKTOP_DIR="$USERDATA/Desktop"
# XDG_DOWNLOAD_DIR="$USERDATA/Downloads"
# XDG_TEMPLATES_DIR="$USERDATA/Templates"
# XDG_PUBLICSHARE_DIR="$USERDATA/Public"
# XDG_DOCUMENTS_DIR="$USERDATA/Documents"
# XDG_MUSIC_DIR="$USERDATA/Music"
# XDG_PICTURES_DIR="$USERDATA/Photos"
# XDG_VIDEOS_DIR="$USERDATA/Video"