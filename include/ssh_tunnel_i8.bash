ssh_tunnel() {
  # Check if the correct number of arguments is provided
  if [ "$#" -ne 2 ]; then
    echo "Usage: ssh_tunnel publicPort<00-99> localPort"
    return 1
  fi

  # Assign arguments to variables
  local PORT1=$1
  local PORT2=$2

  echo "Vezi https://tp523${PORT1}.iopt.ro"

  # Execute the SSH command to create the tunnel
  ssh -N -R 523${PORT1}:localhost:${PORT2} tunnel@tp.iopt.ro
}
