
alias la='ls -lAh --group-directories-first'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

lsd() {
  ls -lAh --group-directories-first "$@"
  echo
  du -sh "$@"
}
