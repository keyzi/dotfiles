#!/bin/bash

resolve_link() {
  local LINK_PATH=$1
  if [ -L "$LINK_PATH" ]; then
    readlink -f "$LINK_PATH"
  else
    echo $LINK_PATH
  fi
}

RESOLVED_BASH_SOURCE=$(resolve_link "${BASH_SOURCE[0]}")
export DOTFILES_DIR=$( cd -- "$( dirname -- "$RESOLVED_BASH_SOURCE" )" &> /dev/null && pwd )

source $DOTFILES_DIR/include/debian_default.bashrc
source $DOTFILES_DIR/include/ssh_tunnel_i8.bash
source $DOTFILES_DIR/include/utils.sh

# Set LS_COLORS environment by Deepin
if [[ ("$TERM" = *256color || "$TERM" = screen* || "$TERM" = xterm* ) && -f /etc/lscolor-256color ]]; then
    eval $(dircolors -b /etc/lscolor-256color)
else
    eval $(dircolors)
fi

# HomeBrew
# if [ -f /home/linuxbrew/.linuxbrew/bin/brew ]; then
#   eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
# fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export CLOCKING_FILE="/srv/Clocking.log"
export CLOCKING_PROJECTS="/srv/projects.json"

alias now="node /srv/Clocking/simpleclocking/main.js"

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# User Path
export PATH=$HOME/.local/bin:$PATH
