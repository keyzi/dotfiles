#!/bin/bash

. "$DOTFILES/include/get_latest_release.sh"

last_version=$(get_latest_release nvm-sh/nvm)
url="https://raw.githubusercontent.com/nvm-sh/nvm/$last_version/install.sh"

curl -o- $url | bash