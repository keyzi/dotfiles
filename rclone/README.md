# Docs
https://github.com/rclone/rclone/wiki/Systemd-rclone-mount#systemd

# astea

```bash
systemctl --user start rclone@pcloud-work
systemctl --user enable rclone@pcloud-work
```
